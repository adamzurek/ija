#!/bin/sh

ulimit -v unlimited

javac -d build/ src/ija/homework3/board/*.java
javac -d build/ -cp build src/ija/homework3/board/Main.java

jar -cfm dest-client/ija2015-client.jar src/META-INF/MANIFEST.MF -C build/ .
java -jar dest-client/ija2015-client.jar
