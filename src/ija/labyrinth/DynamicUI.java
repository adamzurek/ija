package ija.labyrinth;

import ija.labyrinth.board.MazeCard;
import ija.labyrinth.board.MazeCard.CANGO;
import ija.labyrinth.board.MazeField;
import ija.labyrinth.treasure.Treasure;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.nio.file.Paths;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.plaf.basic.BasicArrowButton;

/**
 * @author xzurek13 Adam Žurek
 * @author xbrand06 Zdenko Brandejs
 */
public final class DynamicUI extends javax.swing.JFrame
{
    JLabel[][][] mazeBoard;
    JLabel[][] playersLabels;

    final String LIB_DIR;

    
    /**
     * Creates new form DynamicUI
     */
    public DynamicUI()
    {
        LIB_DIR = Paths.get("lib").toAbsolutePath().toString();
        
        this.mazeBoard = new JLabel[2][Game.getMazeBoard().getSize()][Game.getMazeBoard().getSize() + 1];
        this.playersLabels = new JLabel[Player.getPlayerCount()][6];
        
        loadPlayers();
        loadTreasures();
        loadBoard();
        
        initComponents();
        
        // schovat hrace, kteri nehraji
        switch (Player.getPlayerCount())
        {
            case 2:     // schovat hrace 3 a hrace 4 (bez break!)
                this.player3.setVisible(false);
            case 3:     // schovat hrace 4
                this.player4.setVisible(false);
                break;
        }        
        
        //this.setMinimumSize(new Dimension(Game.getMazeBoard().getSize() * 65 + 200*2, Game.getMazeBoard().getSize() * 65 + (Player.getPlayerCount() > 2 ? 250 : 150)));

        reloadLabels();
    }

    /**
     * Vytvori labely pro poklady
     */
    private void loadTreasures()
    {
        for (int y = 1; y <= Game.getMazeBoard().getSize(); y++) {
            for (int x = 1; x <= Game.getMazeBoard().getSize(); x++) {
                JLabel label = new JLabel();
                label.setBounds(140 + x * 65, y * 65, 60, 60);
                this.mazeBoard[0][y - 1][x - 1] = label;
                this.add(label);
            }
        }

        // freeCard
        JLabel label = new JLabel();
        label.setBounds(20, 250, 60, 60);
        this.mazeBoard[0][Game.getMazeBoard().getSize() - 1][Game.getMazeBoard().getSize()] = label;
        this.add(label);
    }

    /**
     * Vytvori labely pro policka hry
     */
    private void loadBoard()
    {
        ActionListener actionListenerBtn = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionPerformedOnBtn(e);
            }
        };

        MouseListener mouseListenerFreeCard = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Game.getMazeBoard().getFreeCard().turnRight();
                reloadLabels();
            }
        };

        for (int y = 1; y <= Game.getMazeBoard().getSize(); y++) {
            for (int x = 1; x <= Game.getMazeBoard().getSize(); x++) {
                JLabel label = new JLabel();
                label.setBounds(140 + x * 65, y * 65, 60, 60);
                this.mazeBoard[1][y - 1][x - 1] = label;
                this.add(label);

                // shiftovaci sipecky
                if (y == 1 && x % 2 == 0) {                 // shora
                    JButton btn = new BasicArrowButton(BasicArrowButton.SOUTH);
                    btn.setToolTipText("Vsunout volnou kartu shora");
                    btn.setBounds(160 + x * 65, 40, 20, 20);
                    btn.addActionListener(actionListenerBtn);
                    this.add(btn);
                } else if (y == Game.getMazeBoard().getSize() && x % 2 == 0) {  // zdola
                    JButton btn = new BasicArrowButton(BasicArrowButton.NORTH);
                    btn.setToolTipText("Vsunout volnou kartu zdola");
                    btn.setBounds(160 + x * 65, 60 + y * 65, 20, 20);
                    btn.addActionListener(actionListenerBtn);
                    this.add(btn);
                } else if (x == 1 && y % 2 == 0) {          // zleva
                    JButton btn = new BasicArrowButton(BasicArrowButton.EAST);
                    btn.setToolTipText("Vsunout volnou kartu zleva");
                    btn.setBounds(180, 20 + y * 65, 20, 20);
                    btn.addActionListener(actionListenerBtn);
                    this.add(btn);
                } else if (x == Game.getMazeBoard().getSize() && y % 2 == 0) {  // zprava
                    JButton btn = new BasicArrowButton(BasicArrowButton.WEST);
                    btn.setToolTipText("Vsunout volnou kartu zprava");
                    btn.setBounds(200 + x * 65, 20 + y * 65, 20, 20);
                    btn.addActionListener(actionListenerBtn);
                    this.add(btn);
                }
            }
        }

        // freeCard
        JLabel label = new JLabel();
        label.addMouseListener(mouseListenerFreeCard);
        label.setBounds(20, 250, 60, 60);
        this.mazeBoard[1][Game.getMazeBoard().getSize() - 1][Game.getMazeBoard().getSize()] = label;
        this.add(label);
    }

    /** 
     * Vytvori labely pro hracovske komponenty
     */
    private void loadPlayers()
    {           
        MouseListener mouseListenerTurn = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (Player.getWinner() != null) {
                    return;
                }

                Player.nextPlayer();
                reloadLabels();
            }
        };
        
        final Color playerColors[] = new Color[] {
            Color.RED,
            Color.BLUE,
            Color.GREEN,
            Color.WHITE
        };

        ImageIcon bgIconTreasure = new ImageIcon(LIB_DIR + "/cards/bg.png");
        ImageIcon iconTurn = new ImageIcon(LIB_DIR + "/board/na_tahu.png");

        for (int i = 0; i < Player.getPlayerCount(); i++) {
            Player player = Player.getNextPlayer();

            ImageIcon iconTreasure = new ImageIcon(LIB_DIR + "/treasures/POKLAD" + (player.getCard().getCode() + 1) + ".png");
            ImageIcon iconFigure = new ImageIcon(LIB_DIR + "/board/Hrac" + (player.getId() + 1) + ".png");
            
            JLabel name = new JLabel(player.getName(), JLabel.CENTER);
            JLabel card = new JLabel(iconTreasure);
            JLabel bgCard = new JLabel(bgIconTreasure);
            JLabel turn = new JLabel(iconTurn);
            JLabel figure = new JLabel(iconFigure);
            JLabel treasureCaptured = new JLabel("0");

            name.setSize(70, 20);
            bgCard.setSize(70, 107);
            card.setSize(60, 60);
            turn.setSize(70, 70);
            figure.setSize(20, 40);
            treasureCaptured.setSize(20, 20);

            Font f = new Font(name.getFont().getFontName(), name.getFont().getStyle(), 20);
            name.setFont(f);
            name.setForeground(playerColors[player.getId()]);
            figure.setLocation(this.coordToPosition(player.getPosition()));
            turn.addMouseListener(mouseListenerTurn);
            turn.setToolTipText("Kliknutím přepnout na dalšího hráče.");
            treasureCaptured.setForeground(playerColors[player.getId()]);

            this.add(name);
            this.add(card);
            this.add(bgCard);
            this.add(turn);
            this.add(figure);
            this.add(treasureCaptured);

            this.playersLabels[player.getId()][0] = name;
            this.playersLabels[player.getId()][1] = bgCard;
            this.playersLabels[player.getId()][2] = card;
            this.playersLabels[player.getId()][3] = turn;
            this.playersLabels[player.getId()][4] = figure;
            this.playersLabels[player.getId()][5] = treasureCaptured;
        }
    }

    /**
     * Udalost pri vsunuti karticky (shift)
     * @param evt 
     */
    private void actionPerformedOnBtn(ActionEvent evt)
    {
        if (Player.canMove() || Player.getWinner() != null) {
            return;
        }
        
        JButton btn = (JButton)evt.getSource();
        int row, col;

        if (btn.getY() == 40) {     // shora
            row = 1;
            col = (btn.getX() - 160) / 65;
        } else if (btn.getY() == 60 + Game.getMazeBoard().getSize() * 65) {  // zdola
            row = (btn.getY() - 60) / 65;
            col = (btn.getX() - 160) / 65;
        } else if (btn.getX() == 200 + Game.getMazeBoard().getSize() * 65) {  // zprava
            row = (btn.getY() - 20) / 65;
            col = (btn.getX() - 200) / 65;
        } else if (btn.getX() == 180) {                    // zleva
            row = (btn.getY() - 20) / 65;
            col = 1;
        } else {
            System.err.println("fail: (X:" + btn.getX() + ", Y:" + btn.getY() + ")");
            return;
        }

        MazeField mf = Game.getMazeBoard().get(row, col);
        CANGO towards = Game.getMazeBoard().shift(mf);
        
        if (towards == null) {
            return;
        }

        for (int p = 0; p < Player.getPlayerCount(); p++) {
            Player player = Player.getActual();
            MazeField position = player.getPosition();
            if (position.col() == mf.col()) {
                if (towards == CANGO.UP) {
                    if (position.row() == 1) {
                        player.setPosition(Game.getMazeBoard().get(Game.getMazeBoard().getSize(), position.col()));
                    } else {
                        player.setPosition(Game.getMazeBoard().get(position.row() - 1, position.col()));
                    }
                } else if (towards == CANGO.DOWN) {
                    if (position.row() == Game.getMazeBoard().getSize()) {
                        player.setPosition(Game.getMazeBoard().get(1, position.col()));
                    } else {
                        player.setPosition(Game.getMazeBoard().get(position.row() + 1, position.col()));
                    }
                }
            }
            if (position.row() == mf.row()) {
                if (towards == CANGO.LEFT) {
                    if (position.col() == 1) {
                        player.setPosition(Game.getMazeBoard().get(position.row(), Game.getMazeBoard().getSize()));
                    } else {
                        player.setPosition(Game.getMazeBoard().get(position.row(), position.col() - 1));
                    }
                } else if (towards == CANGO.RIGHT) {
                    if (position.col() == Game.getMazeBoard().getSize()) {
                        player.setPosition(Game.getMazeBoard().get(position.row(), 1));
                    } else {
                        player.setPosition(Game.getMazeBoard().get(position.row(), position.col() + 1));
                    }
                }
            }
            Player.getNextPlayer();
        }

        Player.canMove(true);
        this.reloadLabels();
    }

    /**
     * Pomocna funkce na zjisteni policka ve framu
     * @param coord
     * @return 
     */
    private Point coordToPosition(MazeField coord)
    {
        Point p = new Point();
        
        p.x = 140 + coord.col() * 65 + 65/2 - 20/2;
        p.y = coord.row() * 65 + 65/3 - 40/2;

        return p;
    }

    /**
     * Prekresleni ikon labelu
     */
    private void reloadLabels()
    {
        // cesty na kamenech
        for (int y = 1; y <= Game.getMazeBoard().getSize(); y++) {
            for (int x = 1; x <= Game.getMazeBoard().getSize(); x++) {
                MazeCard card = Game.getMazeBoard().get(y, x).getCard();
                ImageIcon icon = new ImageIcon(LIB_DIR + "/stones/" + card.getType() + card.getRotated() + ".png");
                this.mazeBoard[1][y - 1][x - 1].setIcon(icon);
            }
        }

        // poklady na kamenech
        for (int y = 1; y <= Game.getMazeBoard().getSize(); y++) {
            for (int x = 1; x <= Game.getMazeBoard().getSize(); x++) {
                Treasure treasure = Game.getMazeBoard().get(y, x).getCard().getTreasure();
                ImageIcon icon = null;
                if (treasure != null) {
                    icon = new ImageIcon(LIB_DIR + "/treasures/POKLAD" + (treasure.getCode() + 1)  + ".png");
                }
                this.mazeBoard[0][y - 1][x - 1].setIcon(icon);
            }
        }
        
        // zmena pozice hrace
        for (int p = 0; p < Player.getPlayerCount(); p++) {
            Point playerPosition = this.coordToPosition(Player.getActual().getPosition());
            this.playersLabels[Player.getActual().getId()][4].setLocation(playerPosition);
            
            Player.getNextPlayer();
        }
        
        // freeCard
        MazeCard freeCard = Game.getMazeBoard().getFreeCard();
        
        // freeCard poklad
        Treasure freeCardTreasure = freeCard.getTreasure();
        ImageIcon iconFreeCardTreasure = null;
        if (freeCardTreasure != null) {
            iconFreeCardTreasure = new ImageIcon(LIB_DIR + "/treasures/POKLAD" + (freeCardTreasure.getCode() + 1) + ".png");
        }
        this.mazeBoard[0][Game.getMazeBoard().getSize() - 1][Game.getMazeBoard().getSize()].setIcon(iconFreeCardTreasure);

        // freeCard cesticka
        ImageIcon icon = null;
        if  (!Player.canMove()) {   // zobrazit az po prepnuti na dalsiho hrace
            icon = new ImageIcon(LIB_DIR + "/stones/" + freeCard.getType() + freeCard.getRotated() + ".png");
        }
        this.mazeBoard[1][Game.getMazeBoard().getSize() - 1][Game.getMazeBoard().getSize()].setIcon(icon);
        
        for (int i = 0; i < Player.getPlayerCount(); i++) {
            this.playersLabels[i][2].setVisible(false); // karticka
            this.playersLabels[i][3].setVisible(false); // kdo je na tahu
            
            this.playersLabels[i][5].setText("" + Player.getActual().getTreasuresCaptured());
            Player.getNextPlayer();
        }
        // aktualni hrac
        Treasure treasure = Player.getActual().getCard().getTreasure();
        ImageIcon iconTreasure = null;
        if (treasure != null) {
            iconTreasure = new ImageIcon(LIB_DIR + "/treasures/POKLAD" + (treasure.getCode() + 1) + ".png");
        }
        this.playersLabels[Player.getActual().getId()][2].setIcon(iconTreasure);    // zmenit obrazek pokladu na karticce
        this.playersLabels[Player.getActual().getId()][2].setVisible(true);         // zobrazit poklad na karticce
        this.playersLabels[Player.getActual().getId()][3].setVisible(true);         // je na tahu
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        player1 = new javax.swing.JPanel();
        jLabel011 = new javax.swing.JLabel();
        player2 = new javax.swing.JPanel();
        jLabel125 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel142 = new javax.swing.JLabel();
        player3 = new javax.swing.JPanel();
        jLabel122 = new javax.swing.JLabel();
        player4 = new javax.swing.JPanel();
        jLabel127 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        player1.setBackground(new java.awt.Color(255, 204, 204));
        player1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        player1.setPreferredSize(new java.awt.Dimension(92, 188));

        jLabel011.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel011.setForeground(java.awt.Color.red);
        jLabel011.setText("získal:");

        javax.swing.GroupLayout player1Layout = new javax.swing.GroupLayout(player1);
        player1.setLayout(player1Layout);
        player1Layout.setHorizontalGroup(
            player1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(player1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel011)
                .addContainerGap(42, Short.MAX_VALUE))
        );
        player1Layout.setVerticalGroup(
            player1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(player1Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jLabel011)
                .addContainerGap(133, Short.MAX_VALUE))
        );

        player2.setBackground(new java.awt.Color(102, 204, 255));
        player2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        player2.setPreferredSize(new java.awt.Dimension(92, 188));

        jLabel125.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel125.setForeground(java.awt.Color.blue);
        jLabel125.setText("získal:");

        javax.swing.GroupLayout player2Layout = new javax.swing.GroupLayout(player2);
        player2.setLayout(player2Layout);
        player2Layout.setHorizontalGroup(
            player2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(player2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel125)
                .addContainerGap(46, Short.MAX_VALUE))
        );
        player2Layout.setVerticalGroup(
            player2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(player2Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(jLabel125)
                .addContainerGap(124, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(51, 51, 51));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));

        jLabel142.setBackground(new java.awt.Color(102, 102, 102));
        jLabel142.setFont(new java.awt.Font("Calibri", 2, 14)); // NOI18N
        jLabel142.setForeground(new java.awt.Color(239, 239, 31));
        jLabel142.setText("rotující kámen:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel142)
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel142)
                .addContainerGap(80, Short.MAX_VALUE))
        );

        player3.setBackground(new java.awt.Color(51, 51, 51));
        player3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        player3.setPreferredSize(new java.awt.Dimension(92, 188));

        jLabel122.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel122.setForeground(new java.awt.Color(255, 255, 255));
        jLabel122.setText("získal:");

        javax.swing.GroupLayout player3Layout = new javax.swing.GroupLayout(player3);
        player3.setLayout(player3Layout);
        player3Layout.setHorizontalGroup(
            player3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(player3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel122)
                .addContainerGap(46, Short.MAX_VALUE))
        );
        player3Layout.setVerticalGroup(
            player3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(player3Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabel122)
                .addContainerGap(126, Short.MAX_VALUE))
        );

        player4.setBackground(new java.awt.Color(204, 255, 204));
        player4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        player4.setPreferredSize(new java.awt.Dimension(92, 188));

        jLabel127.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel127.setForeground(new java.awt.Color(0, 153, 102));
        jLabel127.setText("získal:");

        javax.swing.GroupLayout player4Layout = new javax.swing.GroupLayout(player4);
        player4.setLayout(player4Layout);
        player4Layout.setHorizontalGroup(
            player4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(player4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel127)
                .addContainerGap(46, Short.MAX_VALUE))
        );
        player4Layout.setVerticalGroup(
            player4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(player4Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabel127)
                .addContainerGap(126, Short.MAX_VALUE))
        );

        jMenuBar1.setBackground(new java.awt.Color(255, 255, 255));
        jMenuBar1.setBorder(null);
        jMenuBar1.setForeground(new java.awt.Color(255, 255, 255));
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jMenuBar1.setPreferredSize(new java.awt.Dimension(100, 22));

        jMenu1.setBackground(new java.awt.Color(255, 255, 255));
        jMenu1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jMenu1.setText("Nová hra");
        jMenu1.setFocusable(false);
        jMenu1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu1MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        jMenu2.setBackground(new java.awt.Color(255, 255, 255));
        jMenu2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jMenu2.setText("Krok zpět");
        jMenu2.setFocusable(false);
        jMenu2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMenuBar1.add(jMenu2);

        jMenu3.setBackground(new java.awt.Color(255, 255, 255));
        jMenu3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jMenu3.setText("Uložit hru");
        jMenu3.setFocusable(false);
        jMenu3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu3);

        jMenu4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jMenu4.setText("Načíst hru");
        jMenu4.setFocusable(false);
        jMenu4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMenu4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu4MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu4);

        jMenu6.setText("Nápověda");
        jMenu6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu6MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu6);

        jMenu5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jMenu5.setText("Zavřít hru");
        jMenu5.setFocusable(false);
        jMenu5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMenu5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu5MouseClicked(evt);
            }
        });
        jMenu5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu5ActionPerformed(evt);
            }
        });
        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(player3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(player1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 565, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(player4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(player2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(player1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(player2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(player3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(player4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Nastaveni pozic vsech komponent ve framu
     * @param evt 
     */
    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        Point positions[] = new Point[]{
            new Point(100, 40),
            new Point(this.getWidth() - 100, 40),
            new Point(100, this.getHeight() - 200),
            new Point(this.getWidth() - 100, this.getHeight() - 200)
        };

        for (int i = 0; i < Player.getPlayerCount(); i++) {
            this.playersLabels[i][0].setLocation(positions[i].x, positions[i].y - 30);      // jmeno
            this.playersLabels[i][1].setLocation(positions[i].x - 10, positions[i].y + 20); // pozadi karty
            this.playersLabels[i][2].setLocation(positions[i].x - 10, positions[i].y + 50); // poklad
            this.playersLabels[i][3].setLocation(positions[i].x - 100, positions[i].y);     // figurka
            this.playersLabels[i][5].setLocation(positions[i].x + 40, positions[i].y);     // figurka
        }
    }//GEN-LAST:event_formComponentResized

    /**
     * Posouvani hracovske figurky
     * @param evt 
     */
    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        if (!Player.canMove() || Player.getWinner() != null) {
            return;
        }
        
        HashMap<Integer, CANGO> playerToTowards = new HashMap<>(4);
        playerToTowards.put(KeyEvent.VK_UP, CANGO.UP);
        playerToTowards.put(KeyEvent.VK_DOWN, CANGO.DOWN);
        playerToTowards.put(KeyEvent.VK_LEFT, CANGO.LEFT);
        playerToTowards.put(KeyEvent.VK_RIGHT, CANGO.RIGHT);
        
        HashMap<CANGO, CANGO> opositeTowards = new HashMap<>(4);
        opositeTowards.put(CANGO.DOWN, CANGO.UP);
        opositeTowards.put(CANGO.UP, CANGO.DOWN);
        opositeTowards.put(CANGO.LEFT, CANGO.RIGHT);
        opositeTowards.put(CANGO.RIGHT, CANGO.LEFT);
        
        CANGO playerTowards = playerToTowards.get(evt.getKeyCode());
        MazeField position = Player.getActual().getPosition();
        
        if (position.getCard().canGo(playerTowards)) {
            MazeField nextF;

            if (playerTowards == CANGO.UP && position.row() > 1) {  // nahoru
                nextF = Game.getMazeBoard().get(position.row() - 1, position.col());
            } else if (playerTowards == CANGO.DOWN && position.row() < Game.getMazeBoard().getSize()) { // dolu
                nextF = Game.getMazeBoard().get(position.row() + 1, position.col());
            } else if (playerTowards == CANGO.LEFT && position.col() > 1) { // doleva
                nextF = Game.getMazeBoard().get(position.row(), position.col() - 1);
            } else if (playerTowards == CANGO.RIGHT && position.col() < Game.getMazeBoard().getSize()) {    // doprava
                nextF = Game.getMazeBoard().get(position.row(), position.col() + 1);
            } else {
                return;
            }
            
            if (nextF.getCard().canGo(opositeTowards.get(playerTowards))) {
                Player.getActual().setPosition(nextF);
            
                if (Player.getActual().getCard() == null) { // konec
                    int max = Player.getActual().getTreasuresCaptured();
                    Player.setWinner(Player.getActual());
                    
                    for (int i = 1; i < Player.getPlayerCount(); i++) {
                        Player act = Player.getActual();
                        if (act.getTreasuresCaptured() > max) {
                            max = act.getTreasuresCaptured();
                            Player.setWinner(act);
                        }
                    }
                    
                    JOptionPane.showMessageDialog(this, "Vyhrál hráč " + Player.getWinner().getName(), "Konec hry", JOptionPane.INFORMATION_MESSAGE);
                }
                
                reloadLabels();
            }
        }
    }//GEN-LAST:event_formKeyReleased

    private void jMenu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseClicked
        NewGame g = new NewGame(this, true);
        g.setLocationRelativeTo(this);
        g.setVisible(true);
    }//GEN-LAST:event_jMenu1MouseClicked

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        if (Player.getWinner() != null) {
            JOptionPane.showMessageDialog(this, "Tato hra je dohrána, nelze uložit.", "Nelze uložit hru", JOptionPane.WARNING_MESSAGE);
        } else {
            SaveGame s = new SaveGame(this, true);
            s.setLocationRelativeTo(this);
            s.setVisible(true);
        }
    }//GEN-LAST:event_jMenu3MouseClicked

    private void jMenu4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu4MouseClicked
        LoadGame g = new LoadGame(this, true);
        g.setLocationRelativeTo(this);
        g.setVisible(true);
    }//GEN-LAST:event_jMenu4MouseClicked

    private void jMenu6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu6MouseClicked
        Help h = new Help(this, true);
        h.setLocationRelativeTo(this);
        h.setVisible(true);
    }//GEN-LAST:event_jMenu6MouseClicked

    private void jMenu5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu5MouseClicked
        ReallyExit r = new ReallyExit(this, true);
        r.setLocationRelativeTo(this);
        r.setVisible(true);
    }//GEN-LAST:event_jMenu5MouseClicked

    private void jMenu5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu5ActionPerformed
        ReallyExit r = new ReallyExit(this, true);
        r.setLocationRelativeTo(this);
        r.setVisible(true);
    }//GEN-LAST:event_jMenu5ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel011;
    private javax.swing.JLabel jLabel122;
    private javax.swing.JLabel jLabel125;
    private javax.swing.JLabel jLabel127;
    private javax.swing.JLabel jLabel142;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel player1;
    private javax.swing.JPanel player2;
    private javax.swing.JPanel player3;
    private javax.swing.JPanel player4;
    // End of variables declaration//GEN-END:variables
}
