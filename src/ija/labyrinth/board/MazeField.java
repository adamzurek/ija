package ija.labyrinth.board;

import java.io.Serializable;

/**
 * @author xzurek13 Adam Žurek
 * @author xbrand06 Zdenko Brandejs
 */
public class MazeField implements Serializable
{
    private int row;
    private int col;
    private MazeCard card;


    public MazeField (int row, int col)
    {
        this(row, col, null);
    }

    public MazeField (int row, int col, MazeCard card)
    {
        this.card = card;
        this.row = row;
        this.col = col;
    }

    public int row ()
    {
        return this.row + 1;    // indexovane od 1
    }

    public int col ()
    {
        return this.col + 1; // indexovane od 1
    }

    public MazeCard getCard ()
    {
        return this.card;
    }

    public void putCard (MazeCard c)
    {
        this.card = c;
    }

    @Override
    public String toString ()
    {
        if (this.card != null) {
            return this.card.toString();
        }

        return "X";
    }
    
    public static boolean coordsEquals (MazeField mf1, MazeField mf2)
    {
        if (mf2 == null) {
            return true;
        }
        if (mf1 == null) {
            return false;
        }
        
        return mf1.row() == mf2.row() && mf1.col() == mf2.col();
    }
}
