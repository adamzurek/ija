package ija.labyrinth.board;


import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;


/**
 * @author xzurek13 Adam Žurek
 * @author xbrand06 Zdenko Brandejs
 */
public class Console
{
    private final static int SIZE = 7;

    public static void main (String[] args)
    {
        PrintWriter out = new PrintWriter(new OutputStreamWriter(System.out, StandardCharsets.UTF_8), true);

        out.println("Vítejte ve hře Labyrinth!\nK dispozici jsou příkazy:\n\tp - zobrazí hrací desku\n\tn - vytvoří novou hru\n\tq - ukončí aplikaci\n\tsRC - vsune volný kámen na pozici políčka [R, C]\n");
        out.flush();

        MazeBoard mb = MazeBoard.createMazeBoard(SIZE);
        Scanner in = new Scanner(System.in);

        while (true) {
            if (!in.hasNextLine()) {
                break;
            }

            String input = in.nextLine();

            if (input.length() == 0) {
                out.println("!Neznámý příkaz!");
                out.println("K dispozici jsou příkazy:\n\tp - zobrazí hrací desku\n\tn - vytvoří novou hru\n\tq - ukončí aplikaci\n\tsRC - vsune volný kámen na pozici políčka [R, C]");
                out.flush();
                continue;
            }

            if (input.equals("p")) {
                out.print(mb.toString());
                out.flush();
            } else if (input.equals("n")) {
                mb.newGame();
            } else if (input.equals("q")) {
                break;
            } else if (input.charAt(0) == 's' && input.length() == 3) {
                int row = Character.getNumericValue(input.charAt(1));
                int col = Character.getNumericValue(input.charAt(2));

                MazeField freeCard = mb.get(row, col);

                mb.shift(freeCard);
            } else {
                out.println("!Neznámý příkaz!");
                out.println("K dispozici jsou příkazy:\n\tp - zobrazí hrací desku\n\tn - vytvoří novou hru\n\tq - ukončí aplikaci\n\tsRC - vsune volný kámen na pozici políčka [R, C]");
                out.flush();
            }
        }

        in.close();
    }
}
