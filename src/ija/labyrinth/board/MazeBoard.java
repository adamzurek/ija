package ija.labyrinth.board;

import ija.labyrinth.Game;
import ija.labyrinth.board.MazeCard.CANGO;
import ija.labyrinth.treasure.Treasure;
import java.io.Serializable;
import java.util.Random;

/**
 * @author xzurek13 Adam Žurek
 * @author xbrand06 Zdenko Brandejs
 */
public class MazeBoard implements Serializable
{
    private final MazeField fields[][];
    private final int size;
    private MazeCard freeCard;
    private MazeField prev;


    public static MazeBoard createMazeBoard (int n)
    {
        return new MazeBoard(n);
    }

    public MazeBoard (int n)
    {
        this.size = n;
        this.fields = new MazeField[n][n];

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                this.fields[row][col] = new MazeField(row, col);
            }
        }
    }

    public void newGame ()
    {
        String types = MazeCard.types;

        // nagenerovat vsechny typy policek + freeCard
        for (int i = 0; types.length() <= this.size * this.size; i++) {
            types += String.valueOf(MazeCard.types.charAt(i % MazeCard.types.length()));
        }

        // odstranit 4 rohove "C"
        for (int i = 0; i < 4; i++) {
            types = types.replaceFirst("[C]", "");
        }

        // odstranit liche "F" po 4 stranach bez 4 rohu
        for (int i = 0; i < (Math.floor(this.size / 2.0) - 1) * 4; i++) {
            types = types.replaceFirst("[F]", "");
        }

        Random rand = new Random();
        int prevTreasure = 0;
        final int treasureEvery = (this.size * this.size) / Game.getPack().size();

        for (int row = 0; row < this.size; row++) {
            for (int col = 0; col < this.size; col++) {
                int fromStart = row * this.size + col;
                Treasure treasure = null;
                MazeCard card;

                if (fromStart - prevTreasure >= treasureEvery) {
                    treasure = Treasure.getRandomTreasure();
                    prevTreasure = fromStart;
                }

                if ((row == 0 || row == this.size - 1) && (col == 0 || col == this.size - 1)) { // rohy
                    card = MazeCard.create("C", treasure);    // dolni pravy (0x rotace)

                    if (!(col == this.size -1 && row == this.size - 1)) {
                        card.turnRight();   // levy dolni (1x rotace)

                        if (row == 0) {     // levy horni ( 2x rotace)
                            card.turnRight();
                        }

                        if (col == this.size - 1) { // pravy horni (3x rotace)
                            card.turnRight();
                        }
                    }
                } else if (((col == 0 || col == this.size - 1) && row % 2 == 0) || ((row == 0 || row == this.size - 1) && col % 2 == 0)) {  // postranni
                    card = MazeCard.create("F", treasure);

                    if (row != this.size - 1) { // posledni rada je spravne natocena
                        card.turnRight();   // leva (1x rotace)

                        if (col != 0) {     // horni ( 2x rotace)
                            card.turnRight();
                        }

                        if (col == this.size - 1) { // prava (3x rotace)
                            card.turnRight();
                        }
                    }
                } else {
                    int index = rand.nextInt(types.length());
                    String type = String.valueOf(types.charAt(index));
                    
                    card = MazeCard.create(type, treasure);
                    types = types.replaceFirst("[" + type + "]", "");
                }
                
                

                this.get(row + 1, col + 1).putCard(card);
            }
        }

        int index = rand.nextInt(types.length());
        String type = String.valueOf(types.charAt(index));

        this.freeCard = MazeCard.create(type, null);
    }

    public MazeField get (int r, int c)
    {
        if (r > this.size || c > this.size || r < 1 || c < 1) {
            return null;
        }

        return this.fields[r - 1][c - 1];
    }

    public MazeCard getFreeCard ()
    {
        return this.freeCard;
    }

    public int getSize()
    {
        return this.size;
    }

    public CANGO shift (MazeField mf)
    {
        MazeField field = this.get(mf.row(), mf.col());
        if (MazeField.coordsEquals(this.prev, field)) {
            return null;
        }
        
        MazeField last = null;

        if (mf.row() == 1 && mf.col()%2 == 0) {                 // posun dolu
            MazeCard prevCard = field.getCard();
            for (int i = 1; i < this.size; i++) {
                MazeCard tmp = this.get(i + 1, mf.col()).getCard();
                last = this.get(i + 1, mf.col());
                last.putCard(prevCard);
                prevCard = tmp;
            }

            field.putCard(this.freeCard); // na prvni vlozit volny kamen
            this.freeCard = prevCard;                     // z posledniho policka vzit kamen
            this.prev = last;
            
            return CANGO.DOWN;
        } else if (mf.row() == this.size && mf.col()%2 == 0) {  // posun nahoru
            MazeCard nextCard = field.getCard();
            for (int i = this.size - 2; i >= 0; i--) {
                MazeCard tmp = this.get(i + 1, mf.col()).getCard();
                last = this.get(i + 1, mf.col());
                last.putCard(nextCard);
                nextCard = tmp;
            }

            field.putCard(this.freeCard); // na posledni vlozit volny kamen
            this.freeCard = nextCard;                             // z prvni policka vzit kamen
            this.prev = last;

            return CANGO.UP;
        } else if (mf.row()%2 == 0 && mf.col() == 1) {  // posun doprava
            MazeCard prevCard = field.getCard();
            for (int i = 1; i < this.size; i++) {
                MazeCard tmp = this.get(mf.row(), i + 1).getCard();
                last = this.get(mf.row(), i + 1);
                last.putCard(prevCard);
                prevCard = tmp;
            }

            field.putCard(this.freeCard); // na prvni vlozit volny kamen
            this.freeCard = prevCard;                     // z posledniho policka vzit kamen
            this.prev = last;
            
            return CANGO.RIGHT;
        } else if (mf.row()%2 == 0 && mf.col() == this.size) {  // posun doleva
            MazeCard nextCard = field.getCard();
            for (int i = this.size - 2; i >= 0; i--) {
                MazeCard tmp = this.get(mf.row(), i + 1).getCard();
                last = this.get(mf.row(), i + 1);
                last.putCard(nextCard);
                nextCard = tmp;
            }

            field.putCard(this.freeCard); // na posledni vlozit volny kamen
            this.freeCard = nextCard;                             // z prvni policka vzit kamen
            this.prev = last;
            
            return CANGO.LEFT;
        }
        
        return null;
    }

    @Override
    public String toString ()
    {
        String toString = "";

        for (MazeField[] field : this.fields) {
            for (int j = 0; j < field.length; j++) {
                toString += field[j].toString();

                if (j < field.length - 1) {
                    toString += " ";
                }
            }

            toString += "\n";
        }

        if (this.freeCard != null) {
            toString += "\nfreeCard: " + this.freeCard.toString() + "\n";
        }

        return toString;
    }
}
