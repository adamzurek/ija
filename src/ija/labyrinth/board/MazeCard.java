package ija.labyrinth.board;

import ija.labyrinth.treasure.Treasure;
import java.io.Serializable;
import java.util.*;


/**
 * @author xzurek13 Adam Žurek
 * @author xbrand06 Zdenko Brandejs
 */
public class MazeCard implements Serializable
{
    public enum CANGO {
        LEFT,
        UP,
        RIGHT,
        DOWN
    }

    private CANGO path[];
    private final String type;
    private int rotated = 1;
    private final Treasure treasure;
    
    public static String types = "CLF";

    
    private MazeCard (String type, Treasure treasure)
    {
        if (type.length() != 1 || !types.contains(type)) {
            throw new IllegalArgumentException("Špatný parametr type. (" + type + ")");
        }

        this.treasure = treasure;
        
        this.path = new CANGO[3];

        this.type = type;

        switch (type) {
            case "C":
                this.path = new CANGO[]{CANGO.LEFT, CANGO.UP};
                break;
            case "L":
                this.path = new CANGO[]{CANGO.LEFT, CANGO.RIGHT};
                break;
            case "F":
                this.path = new CANGO[]{CANGO.LEFT, CANGO.UP, CANGO.RIGHT};
                break;
        }
    }

    public static MazeCard create (String type, Treasure treasure)
    {
        return new MazeCard(type, treasure);
    }

    public String getType ()
    {
        return this.type;
    }

    public int getRotated ()
    {
        return this.rotated;
    }
    
    public Treasure getTreasure()
    {
        return this.treasure;
    }

    public boolean canGo (CANGO dir)
    {
        for (CANGO c : path) {
            if (c == dir) {
                return true;
            }
        }
        
        return false;
    }

    public void turnRight ()
    {
        for (int i = 0; i < this.path.length; i++) {
            CANGO c = this.path[i];

            if (c == CANGO.LEFT) {
                c = CANGO.UP;
            } else if (c == CANGO.UP) {
                c = CANGO.RIGHT;
            } else if (c == CANGO.RIGHT) {
                c = CANGO.DOWN;
            } else {
                c = CANGO.LEFT;
            }

            this.path[i] = c;
        }

        this.rotated = (this.rotated % (this.type.equals("L") ? 2 : 4)) + 1;
    }

    @Override
    public String toString ()
    {
        /**
         ╣ 185
         ║ 186
         ╗ 187
         ╝ 188
         ╚ 200
         ╔ 201
         ╩ 202
         ╦ 203
         ╠ 204
         ═ 205
         */

        HashMap<String, CANGO[]> paths = new HashMap<>(10);

        paths.put("╣", new CANGO[]{CANGO.UP, CANGO.DOWN, CANGO.LEFT});
        paths.put("║", new CANGO[]{CANGO.UP, CANGO.DOWN});
        paths.put("╗", new CANGO[]{CANGO.LEFT, CANGO.DOWN});
        paths.put("╝", new CANGO[]{CANGO.LEFT, CANGO.UP});
        paths.put("╚", new CANGO[]{CANGO.RIGHT, CANGO.UP});
        paths.put("╔", new CANGO[]{CANGO.RIGHT, CANGO.DOWN});
        paths.put("╩", new CANGO[]{CANGO.LEFT, CANGO.UP, CANGO.RIGHT});
        paths.put("╦", new CANGO[]{CANGO.LEFT, CANGO.DOWN, CANGO.RIGHT});
        paths.put("╠", new CANGO[]{CANGO.RIGHT, CANGO.UP, CANGO.DOWN});
        paths.put("═", new CANGO[]{CANGO.LEFT, CANGO.RIGHT});

        for (Map.Entry<String, CANGO[]> i : paths.entrySet()) {
            if (arrayEqual(this.path, i.getValue())) {
                return i.getKey();
            }
        }

        return "-";
    }

    static boolean arrayEqual (CANGO[] arr1, CANGO[] arr2)
    {
        if (arr1.length != arr2.length) {
            return false;
        }

        for (int i = 0; i < arr2.length; i++) {
            if (Arrays.binarySearch(arr1, arr2[i]) < 0) {
                return false;
            }
        }

        return true;
    }
}
