package ija.labyrinth.treasure;

import java.io.Serializable;

/**
 * @author xzurek13 Adam Žurek
 * @author xbrand06 Zdenko Brandejs
 */
public class TreasureCard implements Serializable
{
    private final Treasure tr;

    public TreasureCard (Treasure tr) {
        this.tr = tr;
    }

    public int getCode()
    {
        return this.tr.getCode();
    }
    
    public Treasure getTreasure()
    {
        return this.tr;
    }

    @Override
    public boolean equals (Object tc) {
        if (tc == null || !(tc instanceof TreasureCard)) {
            return false;
        }

        return this.tr.equals(((TreasureCard)tc).tr);
    }

    @Override
    public int hashCode () {
        return this.tr.hashCode();
    }
}
