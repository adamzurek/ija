package ija.labyrinth.treasure;

import java.io.Serializable;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;

/**
 * @author xzurek13 Adam Žurek
 * @author xbrand06 Zdenko Brandejs
 */
public class CardPack implements Serializable
{
    private final List<TreasureCard> pack;

    public CardPack (int maxSize, int initSize) {
        pack = new ArrayList<>(initSize);

        for(int i = 0; i < initSize; i++){
            pack.add(new TreasureCard(Treasure.getTreasure(i)));
        }
    }

    public TreasureCard popCard () {
        if (pack.isEmpty()) {
            return null;
        }

        TreasureCard tmp = pack.get(0);
        pack.remove(tmp);
        return tmp;
    }

    public int size () {
        return pack.size();
    }

    public void shuffle () {
        TreasureCard card;

        for (int i = 0; i < pack.size(); i++){
            Random random = new Random();
            int r = random.nextInt(pack.size());

            card = pack.get(i);
            pack.set(i, pack.get(r));
            pack.set(r, card);
        }
    }
}
