package ija.labyrinth.treasure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

/**
 * @author xzurek13 Adam Žurek
 * @author xbrand06 Zdenko Brandejs
 */
public class Treasure implements Serializable
{
    private final int code;

    private final static int count = 24;
    private static ArrayList<Treasure> pack;
    private static ArrayList<Integer> randomed = new ArrayList<>();

    private Treasure (int code) {
        this.code = code;
    }

    public int getCode()
    {
        return this.code;
    }

    public static void createSet () {
        pack = new ArrayList<>();

        for(int i = 0; i < count; i++) {
            pack.add(new Treasure(i));
        }
    }

    public static Treasure getTreasure (int code) {
        if (code >= pack.size() || code < 0) {
            return null;
        }

        return pack.get(code);
    }
    
    public static Treasure getRandomTreasure()
    {
        if (Treasure.randomed.size() == Treasure.pack.size()) {
            return null;
        }
        
        Random rnd = new Random();
        int index;
        do {
            index = rnd.nextInt(Treasure.pack.size());
        } while (Treasure.randomed.contains(index));
        
        Treasure.randomed.add(index);

        return Treasure.getTreasure(index);
    }
}
