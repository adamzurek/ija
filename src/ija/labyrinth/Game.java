package ija.labyrinth;

import ija.labyrinth.board.MazeBoard;
import ija.labyrinth.treasure.CardPack;
import ija.labyrinth.treasure.Treasure;

/**
 * @author xzurek13 Adam Žurek
 * @author xbrand06 Zdenko Brandejs
 */
public class Game
{
    private static MazeBoard mb;
    private static CardPack pack;


    public static void newGame(int packSize)
    {
        Treasure.createSet();
        Game.pack = new CardPack(packSize, packSize);

        Game.mb.newGame();
    }

    public static MazeBoard getMazeBoard()
    {
        return Game.mb;
    }
    
    public static CardPack getPack()
    {
        return Game.pack;
    }

    public static Player getActualPlayer()
    {
        return Player.getActual();
    }

    public static void createMazeBoard(int size)
    {
        Game.mb = new MazeBoard(size);
    }

    public static void createPlayers(byte playerCount)
    {
        String playerNames[] = new String[]{"Adam", "Zdenko", "Patrícia", "Luboš"};
        
        Player.createSet(playerCount);

        for (byte i = 0; i < playerCount; i++) {
            Player.newPlayer(new Player(playerNames[i]));
        }
    }
    
    public static void loadGame(MazeBoard mb, CardPack pack)
    {
        Game.mb = mb;
        Game.pack = pack;
    }
}
