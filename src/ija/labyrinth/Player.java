package ija.labyrinth;

import ija.labyrinth.board.MazeField;
import ija.labyrinth.treasure.Treasure;
import ija.labyrinth.treasure.TreasureCard;
import java.io.Serializable;

/**
 * @author xzurek13 Adam Žurek
 * @author xbrand06 Zdenko Brandejs
 */
public final class Player implements Serializable
{
    private final String name;
    private TreasureCard card;
    private int id;
    private MazeField position;
    private int treasuresCaptured = 0;

    private static boolean canMove = false;
    private static int actual = 0;
    private static Player[] players;
    private static Player winner;


    public Player(String name)
    {        
        this.name = name;
        Treasure.createSet();

        this.nextCard();
    }

    public String getName()
    {
        return this.name;
    }

    public TreasureCard getCard()
    {
        return this.card;
    }

    public int getId()
    {
        return this.id;
    }
    
    public int getTreasuresCaptured()
    {
        return this.treasuresCaptured;
    }
    
    public MazeField getPosition()
    {
        return this.position;
    }
    
    public MazeField setPosition(MazeField mf)
    {
        if (Game.getMazeBoard().get(mf.row(), mf.col()).getCard().getTreasure() == this.card.getTreasure()) {
            this.treasuresCaptured++;
            this.card = Game.getPack().popCard();
        }
        
        return this.position = mf;
    }

    public TreasureCard nextCard()
    {
        this.card = Game.getPack().popCard();

        return this.getCard();
    }


    public static void createSet(byte count)
    {
        Player.players = new Player[count];
    }

    public static void newPlayer(Player player)
    {
        MazeField playerPositions[] = new MazeField[] {
            Game.getMazeBoard().get(1, 1),
            Game.getMazeBoard().get(1, Game.getMazeBoard().getSize()),
            Game.getMazeBoard().get(Game.getMazeBoard().getSize(), 1),
            Game.getMazeBoard().get(Game.getMazeBoard().getSize(), Game.getMazeBoard().getSize())
        };

        Player.players[Player.actual] = player;
        player.id = Player.actual;
        player.position = playerPositions[Player.actual];

        Player.actual++;
    }

    public static int getPlayerCount()
    {
        return Player.players.length;
    }

    public static Player getActual()
    {
        return Player.players[Player.actual];
    }
    
    public static Player[] getPlayers()
    {
        return Player.players;
    }

    /**
     * Posunout se dal v seznamu
     * @return 
     */
    public static Player getNextPlayer()
    {
        Player.actual = ++Player.actual % Player.players.length;

        return Player.getActual();
    }
    
    /**
     * Dalsi hrac na rade
     * @return 
     */
    public static Player nextPlayer()
    {
        Player.canMove = false;
        
        return Player.getNextPlayer();
    }
    
    public static boolean canMove()
    {
        return Player.canMove;
    }
    
    public static boolean canMove(boolean canMove)
    {
        return Player.canMove = canMove;
    }
    
    public static void loadPlayers(Player[] players, int actual, boolean canMove)
    {
        Player.players = players;
        Player.actual = actual;
        Player.canMove = canMove;
    }
    
    public static Player getWinner()
    {
        return Player.winner;
    }
    
    public static void setWinner(Player player)
    {
        Player.winner = player;
    }
}
